# userstyles

A collection of Stylish stylesheets. Available for simple installation on [UserStyles.org](https://userstyles.org/users/287600) as well

# Style List

- gmail-hide-capsule-crm.css
  - This style hides the CapsuleCRM integration with Gmail. If you have an overzealous Google Apps admin who refuses to "opt you out", this is the style for you
  - [Direct Install](https://userstyles.org/styles/111138/hide-capsule-crm-in-gmail)
- linkedin-full-screen-messages.css
  - This style expands the messages page to fill the screen.
  - [Direct Install](https://userstyles.org/styles/124945/linkedin-full-screen-messages)
- linkedin-hide-invites-people-you-may-know.css
  - This style hides those pesky "invite" cards on the "People You May Know" page on LinkedIn. For when you actually want to just add people you know are already in the system.
  - [Direct Install](https://userstyles.org/styles/111793/linkedin-hide-invites-on-people-you-may-know)
- waffleio-hide-connected-cards.css
  - This style hides the "Connected PR cards" on Waffle.io. Unfortunately can't hide top-level PR cards, but if you're using the Waffle.io "Connects to" syntax in Github then you won't have any top-level PR cards anyway.
  - [Directly install](https://userstyles.org/styles/111706/waffle-io-hide-connected-prs)
- waffleio-simple-dark.css
  - This is a darkened version of [Waffle.io Simpler](https://userstyles.org/styles/109858/waffle-io-simpler)
  - [Direct install](https://userstyles.org/styles/111708/waffle-io-simple-dark)
- kijiji-ca-hide-cruft.css
  - This style hides all the bullshit on the Kijiji.ca website. Easily adaptable to kijiji.com if you want.
  - [Direct install](https://userstyles.org/styles/117173/kijiji-ca-hide-cruft)
- youtube-music-mode-no-thumbs-hide-vids-sfw.css
  - This style hides everything that makes browsing and listening to music on YouTube such a NSFW experience. Player controls are still available, and you can still browse related music but without having to look at a screen full of sex and violence.
  - [Direct Install](https://userstyles.org/styles/124052/youtube-music-mode-no-thumbs-hide-vids-sfw)
- capistranorb-dark-mode.css
  - This is a darkened version of the CapistranoRB.com website.
  - [Direct install](https://userstyles.org/styles/124481/capistranorb-dark-mode)
- national-post-paywall-hopper.css
  - This gets around the ridiculous paywall at the National Post website.
  - [Direct Install](https://userstyles.org/styles/124580/national-post-paywall-hopper)
- slack-dark-and-blue
  - This is a dark theme for slack with blue fonts
  - [Direct Install](https://userstyles.org/styles/111608/dark-and-blue-slack)
- gitlab-diet.css
  - This is a slimmer, less padding-filled version of Gitlab. It's a Work In Progress and most work has thus far concentrated on the Merge Request page.
  - [Direct Install](https://userstyles.org/styles/125375/gitlab-on-a-diet-less-padding-less-info-wip)
- atlassian-better-code-blocks.css
  - The code blocks on Atlassian's products are complete garbage. At least that's my experience with Confluence. Here I try to make them a bit more readable so you don't pull your hair out
  - [Direct Install](https://userstyles.org/styles/128088/atlassian-better-code-blocks-confluence)
- gitlab-boards-hide-backlog.css
  - Hides the backlog column/list on any Gitlab Issue Board
  - [Direct Install](https://userstyles.org/styles/138666/gitlab-issue-boards-hide-backlog)
- gitlab-boards-hide-done.css
  - Hides the done column/list on any Gitlab Issue Board
  - [Direct Install](https://userstyles.org/styles/138669/gitlab-issue-boards-hide-done)

# Contributing

- Make an issue
- Create a merge request which references the issue
  - include some proof that you haven't broken the style
- Wait for review
- On successful review, I'll merge it in
- After merged in, I'll make sure to update the userstyles.org page

# License

See LICENSE file, but in summary:

1. All styles in this repository are released into the public domain.
2. I did the same when uploading to userstyles.org.
3. If you see a discrepancy, make an issue and let me know about it.
